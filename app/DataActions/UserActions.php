<?php

namespace App\DataActions;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use DateTime;
use DateInterval;
use Exception;

class UserActions extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $table = "users";
    protected $hidden = [
        'password',
    ];


    public function login($email, $password){
        $user = User::where('email', $email)->first();
        if($user == null){
            return null;
        }
    	if(password_verify($password, $user->password)){
    		$access_token = rand(1111111111,9999999999);
			$access_token = password_hash($access_token, PASSWORD_DEFAULT);
			$access_token = md5($access_token);
    		$user->access_token = $user->id.$access_token;
    		$user->save();
    		self::updateAccessTokenExpiryDate($user);
    		return $user;
    	}
    	return null;
    }

    public function updateAccessTokenExpiryDate(User $user){
    	if(empty($user->access_token)){
    		throw new Exception('Access token is expiried');
    	}
    	$minutes_to_add = 15;
		$time = new DateTime();
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
		$expiryDate = $time->format('Y-m-d H:i:s');
		$user->token_expiry_date = $expiryDate;
		$user->save();
    }



    private function decrypt(string $encrypted, string $key): string
    {   
        $decoded = base64_decode($encrypted);
        $nonce = mb_substr($decoded, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');
        $ciphertext = mb_substr($decoded, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, null, '8bit');

        $plain = sodium_crypto_secretbox_open(
            $ciphertext,
            $nonce,
            $key
        );
        if (!is_string($plain)) {
            throw new Exception('Invalid MAC');
        }
        sodium_memzero($ciphertext);
        sodium_memzero($key);
        return $plain;
    }

    public function auth($auth){
        $user = User::where('access_token', $auth['access_token'])->first();
        if($user==NULL){
            return null;
        }
        if($user->id != $auth['id']){
            return null;
        }
        // $now = new DateTime();
        // $token_expiry_date = new DateTime($user->token_expiry_date);
        // if($now>$token_expiry_date){
        //     return null;
        // }
        self::updateAccessTokenExpiryDate($user);
        return $user;
    }

    
}