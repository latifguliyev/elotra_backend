<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\City;
use App\Models\State;


class CitiesController extends Controller
{
    //
    public function getAll($id=1, Request $request){
    	$data = City::all();
    	return $data;
    }

    public function getAllStates(){
        $data = State::all();
        return $data;
    }
}
