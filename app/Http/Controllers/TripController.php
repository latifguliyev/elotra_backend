<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Trip;
use App\Models\State;
use App\Models\City;
use App\Models\Country;
use App\Models\GuiderRequest;

use App\DataActions\UserActions;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Trip as TripResource;

use DB;
use Exception;

class TripController extends Controller
{

    private $userActions;

    function __construct(){
         $this->userActions = new UserActions;
    }

    public function getTrips($id=0, Request $request){
        $trips = User::find($id)->trips;
        $result = $trips->toArray();
        $i = 0;
        foreach($trips as $t){
            $result[$i] = $t->toArray();
            $result[$i]['city'] = $t->city;
            $i++;
        }
        return TripResource::collection($trips);
    }

    public function searchTrip($txt){
        $cities = City::where('name', 'like', '%'.$txt.'%')->get();
        $result = array();
        foreach($cities as $c){
            $trips = $c->trips->toArray();
            if($trips != null){
                array_push($result, $trips);
            }
        }
        return $result;
    }

    public function createTrip(Request $request){
        try{
            $validation = $request->validate(['auth'=>'required', 'city_id'=>'required', 'state_id'=>'required',
                                            'date_from'=>'required','date_to'=>'required', 'guider_interest'=>'required']);
            $input = $request->input();
            $trip = new Trip();
            $user = $this->userActions->auth($input['auth']);
            $input['user_id'] = $input['auth']['id'];
            unset($input['auth']);
            return response()->json($trip->createTrip($user, $input));
        }
        catch(Exception $ex){
            return response()->json(['error'=>$ex->getMessage()]);
        }
    }

    public function deleteTrip($user_id, $trip_id, Request $request){
        try{
            $validation = $request->validate(['auth'=>'required']);
            $input = $request->input();
            $user = $this->userActions->auth($input['auth']);
            $trip = Trip::find($trip_id);
            if($trip == null){
                return response()->json(['error'=>'Trip is not exists']);
            }
            return response()->json($trip->deleteTrip($user));
        }
        catch(Exception $ex){
            return response()->json(['error'=>$ex->getMessage()]);
        }
    }

    public function createRequest(Request $request){
        try{
            $validation = $request->validate(['auth'=>'required','trip_id'=>'required','message'=>'required']);
            $input = $request->input();
            $user = $this->userActions->auth($input['auth']);
            $trip = Trip::find($input['trip_id']);
            unset($input['auth']);
            $guideRequest = new GuiderRequest();
            return response()->json($guideRequest->createRequest($user, $trip, $input));
        }
        catch(Exception $ex){
            return response()->json(['error'=>$ex->getMessage()]);
        }
    }

    public function getRequests($id){
        $trip = Trip::find($id);
        return $trip->guideRequests;
    	return new UserResource($data);
    }

    public function changeStatusOfRequest2($request_id, $status, Request $request){
        try{
            $validation = $request->validate(['auth'=>'required']);
            $input = $request->input();
            $user = $this->userActions->auth($input['auth']);
            if($user == null){
                return ['error'=>'Auth error'];
            }
            print_r($guideRequest = $user->guideRequests);
            $guideRequest = $user->guideRequests->where('id', $request_id)->first();
            if($guideRequest == null){
                return response()->json(['error'=>'You don\'t have this request']);
            }
            //($user, $status){
            echo "OK";
            return response()->json($guideRequest->changeStatusOfRequest($user, $status));
        }
        catch(Exception $ex){
            echo "NO";
            return response()->json(['error'=>$ex->getMessage()]);
        }
    }

    public function changeStatusOfRequest($request_id, $status, Request $request){
        try{
            $validation = $request->validate(['auth'=>'required']);
            $input = $request->input();
            $user = $this->userActions->auth($input['auth']);
            $guideRequest = GuiderRequest::find($request_id);
            if($guideRequest == null){
                return response()->json(['error'=>'Request is not exists']);
            }
            return response()->json($guideRequest->changeStatusOfRequest($user, $status));
        }
        catch(Exception $ex){
            echo "NO";
            return response()->json(['error'=>$ex->getMessage()]);
        }
    }


    public function startTrip($trip_id, Request $request){
        try{
            $validation = $request->validate(['auth'=>'required']);
            $input = $request->input();
            $trip = Trip::find($trip_id);
            if($trip == null){
                return response()->json(['error' => 'Trip not found']);
            }
            $user = $this->userActions->auth($input['auth']);
            return response()->json($trip->startTrip($user));
        }
        catch(Exception $ex){
            return response()->json(['error' => $ex->getMessage()]);
        }
        
    }

    public function finishTrip($trip_id, Request $request){
        try{
            $validation = $request->validate(['auth'=>'required']);
            $input = $request->input();
            $trip = Trip::find($trip_id);
            if($trip==null){
                return response()->json(['error'=>'Trip not found']);
            }
            $user = $this->userActions->auth($input['auth']);
            return response()->json($trip->finishTrip($user));
        }
        catch(Exception $ex){
            return response()->json(['error'=> $ex->getMessage()]);
        }
    }

    public function randomTrips(){
        $trips=Trip::all()->random(3);
        $result = array();
        $i = 0;
        foreach($trips as $t){
            $result[$i] = $t->toArray();
            $result[$i]['city'] = $t->city;
            $i++;
        }
        return $result;
    }
}
