<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Trip;
use App\Models\State;
use App\Models\City;
use App\Models\Country;
use App\Models\GuiderRequest;
use App\Models\Location;

use App\DataActions\UserActions;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Trip as TripResource;
use Illuminate\Support\Facades\Validator;

use DB;
use Exception;

class UserController extends Controller
{

    private $userActions;
    function __construct(){
         $this->userActions = new UserActions;
    }
    //
    public function getUser($id=0, Request $request){
        $user = User::find($id);
    	return $user;
    }

   public function getGuidedTrips($id){
        $user = User::find($id);
        if($user == null){
            return response()->json(['error'=>'User is not exists'], 401);
        }
        // print_r($user);
        $acceptedGuideRequests = $user->guideRequests->where('accepted', 1);
        $trips = array();
        $i=0;
        foreach($acceptedGuideRequests as $tmp){
            $trips[$i] = $tmp->trip;
            $trips[$i]['city'] = $tmp->trip->city;
            $i++;
        }
        return $trips;
   }

    public function searchGuider($txt){
        $countries = Country::where('name', 'like', '%'.$txt.'%')->get();
        $result = array();
        foreach($countries as $c){
            $guiders = $c->users->where('local_guider', 1)->toArray();
            if($guiders != null){
                array_push($result, $guiders);
            }
        }
        return $result;
        //return $city;
    }

    public function login(Request $request){
    	$validation = $request->validate(['email'=>'required','password'=>'required']);
       
        $result = $this->userActions->login($request->input('email'), $request->input('password'));
        return ($result != null) ? $result : response()->json(['error'=>'Username or password is not correct'], 401);
    }

    public function register(Request $request){
        $validator = Validator::make($request->all(), 
                ['name'=>'required','surname'=>'required', 'email'=>'required', 'gender_id'=>'required', 'password'=>'required']);
        if ($validator->fails()){
            return response()->json(['error'=>'Form Validation Failed']);
        }
        $user = new User;
        try{
            DB::transaction(function() use ($user, $request) {
                $user->fill($request->input());
                $user->password=password_hash($request->input('password'), PASSWORD_DEFAULT);
                $user->save();
            });
            return response()->json(['success'=>'User registered']);
        }
        catch(Exception $ex){
            return response()->json(['error'=>'This email registered']);
        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), 
        ['auth'=>'required']);
        if ($validator->fails()){
            return response()->json(['error'=>'Form Validation Failed']);
        }
        $input = $request->input();
        $user = $this->userActions->auth($input['auth']);
        if($user != null){
            unset($input['auth']);
            $user->fill($input);
            if(isset($input['password'])){
                $user->password=password_hash($input['password'], PASSWORD_DEFAULT);
            }
            $user->save();
            return response()->json(['success'=>'Changed']);
        }
        else{
            return response()->json(['error'=>'Auth error']);
        }
    }

    public function statistics(){
        $user_count = User::all()->count();
        $trip_count = Trip::all()->count();
        $location_count = Location::all()->count();
        return response()->json(['user_count'=>$user_count, 'trip_count'=>$trip_count, 'location_count'=>$location_count]);
    }


}
