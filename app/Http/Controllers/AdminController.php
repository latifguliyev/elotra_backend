<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Location;
use Illuminate\Support\Facades\Validator;


class AdminController extends Controller
{
    //
    public function login(Request $request){
        // return ($request->input());
        
        $validator = Validator::make($request->all(), ['username'=>'required','password'=>'required']);
        if (!$validator->fails()) {
            $result = Admin::login($request->input('username'), $request->input('password'));
            return ($result != null) ? $result : response()->json(['error'=>'Username or password is not correct'], 401);
        }
        else{
            return response()->json(['error'=>'Form is not correct'], 401);
        }
    }

    public function locations(Request $request){
        $validator = Validator::make($request->all(), ['auth'=>'required']);
        if (!$validator->fails()) {
            $input = $request->input();
            $admin = Admin::auth($input['auth']);
            if($admin == null){
                return response()->json(['error'=>'Auth error']);
            }
            return $admin->locations;
        }
        else{
            return response()->json(['error'=>'Form is not correct'], 401);
        }
    }

    public function new(Request $request){
        $validator = Validator::make($request->all(), ['auth'=>'required']);
        if (!$validator->fails()) {
            $input = $request->input();
            $admin = Admin::auth($input['auth']);
            unset($input['auth']);
            if($admin == null){
                return response()->json(['error'=>'Auth error']);
            }
            return response()->json($admin->createLocation($input));
            
        }
        else{
            return response()->json(['error'=>'Form is not correct'], 401);
        }
    }

    public function delete(Request $request){
        $validator = Validator::make($request->all(), ['auth'=>'required', 'location_id'=>'required']);
        if (!$validator->fails()) {
            $input = $request->input();
            $admin = Admin::auth($input['auth']);
            unset($input['auth']);
            if($admin == null){
                return response()->json(['error'=>'Auth error']);
            }
            return response()->json($admin->deleteLocation($input['location_id']));
        }
        else{
            return response()->json(['error'=>'Form is not correct'], 401);
        }
    }

    public function edit($location_id, Request $request){
        $validator = Validator::make($request->all(), ['auth'=>'required']);
        if (!$validator->fails()) {
            $input = $request->input();
            $admin = Admin::auth($input['auth']);
            unset($input['auth']);
            if($admin == null){
                return response()->json(['error'=>'Auth error']);
            }
            return response()->json($admin->updateLocation($location_id, $input));
        }
        else{
            return response()->json(['error'=>'Form is not correct'], 401);
        }
    }
  

    public function allUsers(Request $request){
        $validator = Validator::make($request->all(), ['auth'=>'required']);
        if (!$validator->fails()) {
            $input = $request->input();
            $admin = Admin::auth($input['auth']);
            unset($input['auth']);
            if($admin == null){
                return response()->json(['error'=>'Auth error']);
            }
            if($admin->create_admin!=1){
                return response()->json(['error'=>'You don\'t have permission for this request']);
            }
            return response()->json(Admin::all());
        }
        else{
            return response()->json(['error'=>'Form is not correct'], 401);
        }
    }

    public function newUser(Request $request){
        $validator = Validator::make($request->all(), ['auth'=>'required', 'password' => 'required|min:7', 'name'=>'required', 
                                        'username'=>'required', 'create_admin'=>'required']);
        if (!$validator->fails()) {
            $input = $request->input();
            $admin = Admin::auth($input['auth']);
            unset($input['auth']);
            if($admin == null){
                return response()->json(['error'=>'Auth error']);
            }
            return response()->json($admin->newAdmin($input));
        }
        else{
            return response()->json(['error'=>'Form is not correct'], 401);
        }
    }

    public function editUser(Request $request){
        $validator = Validator::make($request->all(), ['auth'=>'required', 'id'=>'required']);
        if (!$validator->fails()) {
            $input = $request->input();
            $admin = Admin::auth($input['auth']);
            unset($input['auth']);
            if($admin == null){
                return response()->json(['error'=>'Auth error']);
            }
            return response()->json($admin->updateAdmin($input));
        }
        else{
            return response()->json(['error'=>'Form is not correct'], 401);
        }
    }
    
}
