<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\DataActions\UserActions;
use App\Models\User;
use App\Models\City;
use App\Models\Country;
use App\Models\State;

use DB;
use DateTime;

class Trip extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $fillable = [
    	'user_id', 'city_id', 'country_id', 'state_id', 'date_from', 'date_to', 'guider_interest', 'number_of_travellers'
    ];

    public function visitor(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function city(){
        return $this->belongsTo('App\Models\City');
    }

    public function country(){
        return $this->belongsTo('App\Models\Country');
    }

    public function state(){
        return $this->belongsTo('App\Models\State');
    }

    public function genderInterest(){
        return $this->belongsTo('App\Models\Gender', 'guider_interest');
    }

    public function guideRequests(){
        return $this->hasMany('App\Models\GuiderRequest');
    }

    public function createTrip($user, $fields){
        if($user == null){
            return ['error'=>'Auth error'];
        }
        try{
            DB::transaction(function() use ($fields) {
                $this->fill($fields);
                $this->save();
            });
            return ['success'=>'Trip created'];
        }
        catch(Exception $ex){
            return ['error'=>$ex->getMessage()];
        }
    }

    public function deleteTrip($user){
        if($user == null){
            return ['error'=>'Auth error'];
        }
        try{
            DB::transaction(function(){
                $this->delete();
            });
            return ['success'=>'Trip Deleted'];
        }
        catch(Exception $ex){
            return ['error'=>$ex->getMessage()];
        }
    }

    public function startTrip($user){
        if($user == null){
            return ['error'=>'Auth error'];
        }
        if($user->id != $this->user_id){
            return ['error'=>'User doesn\'t have this trip!!!'];
        }
        $acceptedRequest = $this->guideRequests->where('accepted', 1)->first();
        if($acceptedRequest == null){
            return ['error'=>'You don\'t have any accepted request to start trip!!!'];
        }
        if($this->start_time != null){
            return ['error'=>'This trip has started before!!!'];
        }
        if($this->end_time != null){
            return ['error'=>'This trip is finished!!!'];
        }
        try{
            DB::transaction(function() {
                $now = new DateTime();
                $this->start_time = $now;
                $this->save();
            });
            return ['success'=>'Trip Started'];
        }
        catch(Exception $ex){
            return ['error'=>$ex->getMessage()];
        }
    }

    public function finishTrip($user){
        if($user == null){
            return ['error'=>'Auth error'];
        }
        $guiderRequest = $this->guideRequests->where('guider_id', $user->id)
        ->where('accepted', 1)->first();
        if($guiderRequest == null){
            return ['error'=>'You haven\'t requested for this trip or Trip owner has not accepted your reqeust'];
        }
        if($this->start_time == null){
            return ['error'=>'This trip has not started before!!!'];
        }
        if($this->end_time != null){
            return ['error'=>'This trip is finished before!!!'];
        }
        try{
            $now = new DateTime();
            $startDate = new DateTime($this->start_time);
            $this->end_time = $now;
            /* Bill Calculation */
            $duration= $now->diff($startDate);
            $minutes = $duration->days * 24 * 60;
            $minutes += $duration->h * 60;
            $minutes += $duration->i;
            $this->bill = round(($minutes * $user->hourly_rate)/60, 2);
            /* ./Bull Calculation */

            /* Point Calculation */
            $numberOfTrips = count(
                $this->visitor->trips->filter(function($trip){
                return $trip['start_time'] != null && $trip['end_time'] != null;
            })->toArray());
            $maxPoint = $numberOfTrips*50 + 50;
            $this->point = rand($maxPoint/2, $maxPoint);
            /* ./Point Calculation */
            
            DB::transaction(function() {
                $this->save();
            });
            return ['success'=>'Trip Finished'];
        }
        catch(Exception $ex){
            return ['error'=>$ex->getMessage()];
        }
    }
}
