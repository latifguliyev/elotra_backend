<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //

    protected $fillable = ['name', 'description', 'required_point', 'lat', 'lng'];

    public function Admin(){
        return $this->belongsTo('App\Models\Admin');
    }
}
