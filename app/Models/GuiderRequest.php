<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

class GuiderRequest extends Model
{
    //
    protected $fillable = ['guider_id','trip_id','message'];

    public function trip(){
        return $this->belongsTo('App\Models\Trip');
    }

    public function guider(){
        return $this->belongsTo('App\Models\User', 'guider_id');
    }

    public function createRequest($user, $trip, $requestContent){
        if($user == null){
            return ['error'=>'Auth error'];
        }
        if($trip->user_id == $user->id){
            return ['error'=>'You can not send request to yourself'];
        }
        $checkRequest = $user->guideRequests->where('trip_id', $trip->id)->toArray();
        if(count($checkRequest)!=0){
            return ['error'=>'You have sent request for this trip'];
        }
        try{
            DB::transaction(function() use ($requestContent, $user)  {
                $requestContent['guider_id'] = $user->id;
                $this->fill($requestContent);
                $this->save();
            });
            return ['success'=>'Request sent'];
        }
        catch(Exception $ex){
            return ['error'=>$ex->getMessage()];
        }
    }

    public function changeStatusOfRequest($user, $status){
        try{
            if($user == null){
                return ['error'=>'Auth error'];
            }
            if($this->trip->user_id != $user->id){
                return ['error'=>'You don\'t have this trip'];
            }
            if($status == 1){
                //check any request accepted for trip or not
                $acceptedRequest = $this->trip->guideRequests->where('accepted', 1)->first();
                if($acceptedRequest != null){
                    return ['error'=>'You have accepted a request for this trip.'];
                }
            }
            DB::transaction(function() use ($status) {
                $this->accepted = $status;
                $this->save();
            });
            return ['success'=>'Request status changed'];
        }
        catch(Exception $ex){
            return ['error'=>$ex->getMessage()];
        }
    }
}
