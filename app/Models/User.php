<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\DataActions\UserActions;

class User extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $fillable = [
    	'name', 'surname', 'email', 'date_of_birth', 'gender_id', 'phone_number', 'country_id', 'state_id','about', 'local_guider','motto', 'hourly_rate','hourly_rate_free',
    	'minimum_tour_hour'
    ];


    public function trips(){
        return $this->hasMany('App\Models\Trip');
    }

    public function guideRequests(){
        return $this->hasMany('App\Models\GuiderRequest', 'guider_id');
    }

    public function gender(){
        return $this->belongsTo('App\Models\Gender');
    }

    public function country(){
        return $this->belongsTo('App\Models\Country');
    }

    public function state(){
        return $this->belongsTo('App\Models\State');
    }
    

}
