<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Location;

use DateTime;
use DateInterval;
use Config;
use DB;

class Admin extends Model
{
    protected $hidden = [
        'password', 'deleted', 'delete_date', 'active', 'updated_at', 'token_expiry_date'
    ];

    protected $fillable = [
        'name', 'username', 'create_admin'
    ];

    public function locations(){
        return $this->hasMany('App\Models\Location');
    }

    //
    public static function login($username, $password){
        $user = Admin::where('username', $username)->first();
        if($user == null){
            return null;
        }
        //$password = Admin::decrypt($password);
    	if(password_verify($password, $user->password)){
    		$access_token = rand(1111111111,9999999999);
			$access_token = password_hash($access_token, PASSWORD_DEFAULT);
			$access_token = md5($access_token);
    		$user->access_token = $user->id.$access_token;
    		$user->save();
    		Admin::updateAccessTokenExpiryDate($user);
    		return $user;
    	}
    	return null;
    }

    public static function updateAccessTokenExpiryDate(Admin $user){
    	if(empty($user->access_token)){
    		throw new Exception('Access token is expiried');
    	}
    	$minutes_to_add = 15;
		$time = new DateTime();
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
		$expiryDate = $time->format('Y-m-d H:i:s');
		$user->token_expiry_date = $expiryDate;
		$user->save();
    }

    public static function auth($auth){
        $user = Admin::where('access_token', $auth['access_token'])->first();
        if($user==NULL){
            return null;
        }
        if($user->id != $auth['id']){
            return null;
        }
        // $now = new DateTime();
        // $token_expiry_date = new DateTime($user->token_expiry_date);
        // if($now>$token_expiry_date){
        //     return null;
        // }
        Admin::updateAccessTokenExpiryDate($user);
        return $user;
    }

    private static function decrypt(string $encrypted): string{
        try{
            $decoded = base64_decode($encrypted);
            $nonce = mb_substr($decoded, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');
            $ciphertext = mb_substr($decoded, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, null, '8bit');

            $plain = sodium_crypto_secretbox_open(
                $ciphertext,
                $nonce,
                Config::get('constants.options.KEY')
            );
            if (!is_string($plain)) {
                throw new Exception('Invalid MAC');
            }
            sodium_memzero($ciphertext);
            // sodium_memzero(Config::get('constants.options.KEY'));
            return $plain;
        }
        catch(Exception $ex){
            return 'Security key is not true';
        }
        
    }

    public function createLocation($input){
        $location = new Location;
        $location->admin_id = $this->id;
        $location->fill($input);
        try{
            DB::transaction(function() use ($location) {
                $location->save();
            });
            return ['success'=>'Location created'];
        }
        catch(Exception $ex){
            return ['error'=>$ex->getMessage()];
        }
    }

    public function deleteLocation($id){
        $location = $this->locations->where('id', $id)->first();
        if($location == null){
            return ['error'=>'You don\'t have this location'];
        }
        try{
            DB::transaction(function() use ($location) {
                $location->delete();
            });
            return ['success'=>'Location deleted'];
        }
        catch(Exception $ex){
            return ['error'=>$ex->getMessage()];
        }
    }

    public function updateLocation($id, $input){
        $location = $this->locations->where('id', $id)->first();
        if($location == null){
            return ['error'=>'You don\'t have this location'];
        }
        try{
            $location->fill($input);
            DB::transaction(function() use ($location) {
                $location->save();
            });
            return ['success'=>'Location updated'];
        }
        catch(Exception $ex){
            return ['error'=>$ex->getMessage()];
        }
    }

    public function newAdmin($input){
        if($this->create_admin!=1){
            return ['error'=>'You don\'t have permission for this request'];
        }
        $admin = Admin::where('username', $input['username'])->first();
        if($admin != null){
            return ['error'=>'This username has an account'];
        }
        $admin = new Admin;
        $admin->password=password_hash($input['password'], PASSWORD_DEFAULT);
        $admin->fill($input);
        try{
            DB::transaction(function() use ($admin) {
                $admin->save();
            });
            return ['success'=>'New user created'];
        }
        catch(Exception $ex){
            return ['error'=>$ex->getMessage()];
        }
    }

    public function updateAdmin($input){
        if($this->create_admin!=1){
            return ['error'=>'You don\'t have permission for this request'];
        }

        $admin = Admin::find($input['id'])->first();
        if($admin == null){
            return ['error'=>'This username doesn\'t exists'];
        }

        if(array_key_exists('username', $input)){
            return ['error'=>'You can not change the username'];
        }

        $admin = new Admin;
        $admin->password=password_hash($input['password'], PASSWORD_DEFAULT);
        $admin->fill($input);
        try{
            DB::transaction(function() use ($admin) {
                $admin->save();
            });
            return ['success'=>'New user created'];
        }
        catch(Exception $ex){
            return ['error'=>$ex->getMessage()];
        }
    }
}
