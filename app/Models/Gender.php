<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    //

    public function interestedTrips(){
        return $this->hasMany('App\Models\Trip');
    }

    public function users(){
        return $this->hasMany('App\Models\User');
    }
}
