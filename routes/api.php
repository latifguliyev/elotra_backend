<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/user/{id?}', ['uses' =>'UserController@getUser'])->where('id', '[0-9]+');
Route::post('/user/login', 'UserController@login');
Route::post('/user/register', 'UserController@register');
Route::post('/user/update', 'UserController@update');
Route::get('/guiders/search/{text}', ['uses' =>'UserController@searchGuider']);
Route::get('/user/{id?}/guides/', ['uses' =>'UserController@getGuidedTrips'])->where('id', '[0-9]+');
Route::get('/statistics/', ['uses' =>'UserController@statistics']);


Route::get('/cities/', ['uses' =>'CitiesController@getAll']);
Route::get('/states/', ['uses' =>'CitiesController@getAllStates']);


//trips
Route::get('/user/{id?}/trips/', ['uses' =>'TripController@getTrips'])->where('id', '[0-9]+');
Route::post('/user/{id?}/create_trip', 'TripController@createTrip')->where('id', '[0-9]+');
Route::post('/user/{id?}/delete_trip/{trip_id?}', 'TripController@deleteTrip')->where('id', '[0-9]+')->where('trip_id', '[0-9]+');


Route::get('/trips/search/{text}', ['uses' =>'TripController@searchTrip']);
Route::post('trips/create_request', 'TripController@createRequest');
Route::get('/trips/random/', ['uses' =>'TripController@randomTrips']);
Route::get('/trips/{id?}/requests', ['uses' =>'TripController@getRequests'])->where('id', '[0-9]+');
Route::post('requests/{request_id?}/change_status/{status}', 'TripController@changeStatusOfRequest')->where('request_id', '[0-9]+')->where('status','[0-1]');//0-reject 1-accept
Route::post('/trips/{id?}/start', 'TripController@startTrip')->where('id', '[0-9]+');
Route::post('/trips/{id?}/finish', 'TripController@finishTrip')->where('id', '[0-9]+');



//Places
Route::post('/admins/login', 'AdminController@login');
Route::post('/admin/locations', 'AdminController@locations');
Route::post('/admin/locations/new/', 'AdminController@new');
Route::post('/admin/locations/delete/', 'AdminController@delete');
Route::post('/admin/locations/{id?}/edit/', 'AdminController@edit')->where('id', '[0-9]+');

//Admins
Route::post('/admin/all_users/', 'AdminController@allUsers');
Route::post('/admin/new/', 'AdminController@newUser');
Route::post('/admin/edit/', 'AdminController@editUser');



//Route::apiResource('/user/', 'UserController');

//Route::get('user/{id?}', 'UserController@getUser', ['user_id'=>0])->where('id', '[0-9]+');
